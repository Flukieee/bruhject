package finalnagidniya;

import java.util.*;

public class Game {

    public static void main(String[] args) {
        System.out.println("Beach Volleyball Game (for Fun)");
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        Profile prof = new Profile("", "");
        
        List<String> roles = new ArrayList<>();
        roles.add("Spiker");
        roles.add("Blocker");
        roles.add("Spiker and Blocker");
        //Team1
        
        System.out.println("First Team");
        System.out.println("Put your Teamname:");
        String Firstteam = scanner.next().toUpperCase();
        System.out.println("Enter the nickname of First player");
        String Team1Firstplayername = scanner.next().toUpperCase();
        System.out.println("Enter the role of First Player'. ");
        System.out.println("'S' for Spiker,'B' for Blocker, any key for both'");
        String Team1Firstplayerrole = scanner.next().toUpperCase();
        if (Team1Firstplayerrole.equals("S")) {
            Team1Firstplayerrole = roles.get(0);

        } else if (Team1Firstplayerrole.equals("B")) {
            Team1Firstplayerrole = roles.get(1);

        } else {
            Team1Firstplayerrole = roles.get(2);

        }

        System.out.println("Enter the nickname of Second player");
        String Team1Secondplayername = scanner.next().toUpperCase();
        System.out.println("Enter the role of Second Player");
        System.out.println("'S' for Spiker,'B' for Blocker, any key for both'");
        String Team1Secondplayerrole = scanner.next().toUpperCase();
        if (Team1Secondplayerrole.equals("S")) {

            Team1Secondplayerrole = roles.get(0);

        } else if (Team1Secondplayerrole.equals("B")) {

            Team1Secondplayerrole = roles.get(1);

        } else {

            Team1Secondplayerrole = roles.get(2);

        }

        System.out.println("");
        //Team2
        System.out.println("Second Team");
        System.out.println("Put your Teamname:");
        String Secondteam = scanner.next().toUpperCase();
        System.out.println("Enter the nickname of First player");
        String Team2Firstplayername = scanner.next().toUpperCase();
        System.out.println("Enter the role of First player");
        System.out.println("'S' for Spiker,'B' for Blocker, any key for both'");
        String Team2Firstplayerrole = scanner.next().toUpperCase();
        if (Team2Firstplayerrole.equals("S")) {

            Team2Firstplayerrole = roles.get(0);

        } else if (Team2Firstplayerrole.equals("B")) {

            Team2Firstplayerrole = roles.get(1);

        } else {

            Team2Firstplayerrole = roles.get(2);

        }
        System.out.println("Enter the nickname of Second player");
        String Team2Secondplayername = scanner.next().toUpperCase();
        System.out.println("Enter the role of Second player");
        System.out.println("'S' for Spiker,'B' for Blocker, any key for both'");
        String Team2Secondplayerrole = scanner.next().toUpperCase();
        if (Team2Secondplayerrole.equals("S")) {

            Team2Secondplayerrole = roles.get(0);

        } else if (Team2Secondplayerrole.equals("B")) {

            Team2Secondplayerrole = roles.get(1);

        } else {

            Team2Secondplayerrole = roles.get(2);

        }

        Teamname team1 = new Teamname(Firstteam);
        Teamname team2 = new Teamname(Secondteam);

        Profile player1 = new Profile(Team1Firstplayername, Team1Firstplayerrole);
        Profile player2 = new Profile(Team1Secondplayername, Team1Secondplayerrole);
        Profile player3 = new Profile(Team2Firstplayername, Team2Firstplayerrole);
        Profile player4 = new Profile(Team2Secondplayername, Team2Secondplayerrole);

        //SE team
        prof.addFirstTeamPlayerProfile(player1);
        prof.addFirstTeamPlayerProfile(player2);
        //Karasuno team
        prof.addSecondTeamPlayerProfile(player3);
        prof.addSecondTeamPlayerProfile(player4);

        //All players
        prof.addAllTeamPlayersProfile(player1);
        prof.addAllTeamPlayersProfile(player2);
        prof.addAllTeamPlayersProfile(player3);
        prof.addAllTeamPlayersProfile(player4);

        System.out.println("T E A M S : ");
        System.out.println("");
        System.out.println(prof.toString());
        //-------
        Queue<Points> totalpointsqueue = new PriorityQueue<>();
        int player1totalpoints = 0;
        int player2totalpoints = 0;
        int player3totalpoints = 0;
        int player4totalpoints = 0;

        Queue<Spikes> spikesqueue = new PriorityQueue<>();
        int player1spikes = 0;
        int player2spikes = 0;
        int player3spikes = 0;
        int player4spikes = 0;

        Queue<Blocks> blocksqueue = new PriorityQueue<>();
        int player1blocks = 0;
        int player2blocks = 0;
        int player3blocks = 0;
        int player4blocks = 0;

        Queue<Serves> servesqueue = new PriorityQueue<>();
        int player1serves = 0;
        int player2serves = 0;
        int player3serves = 0;
        int player4serves = 0;

        int game = 1;
        System.out.println("");
        System.out.println(team1.getTeamname() + " vs. " + team2.getTeamname());
        System.out.println("");

        while (true) {
            System.out.println("E to end and show stats, S to start");
            String decide = scanner.next().toUpperCase();

            Points Player1totalpointstat = new Points(player1.getNickname(), player1totalpoints);
            Points Player2totalpointstat = new Points(player2.getNickname(), player2totalpoints);
            Points Player3totalpointstat = new Points(player3.getNickname(), player3totalpoints);
            Points Player4totalpointstat = new Points(player4.getNickname(), player4totalpoints);

            Spikes Player1spikesstat = new Spikes(player1.getNickname(), player1spikes);
            Spikes Player2spikesstat = new Spikes(player2.getNickname(), player2spikes);
            Spikes Player3spikesstat = new Spikes(player3.getNickname(), player3spikes);
            Spikes Player4spikesstat = new Spikes(player4.getNickname(), player4spikes);

            Blocks Player1blocksstat = new Blocks(player1.getNickname(), player1blocks);
            Blocks Player2blocksstat = new Blocks(player2.getNickname(), player2blocks);
            Blocks Player3blocksstat = new Blocks(player3.getNickname(), player3blocks);
            Blocks Player4blocksstat = new Blocks(player4.getNickname(), player4blocks);

            Serves Player1acesstat = new Serves(player1.getNickname(), player1serves);
            Serves Player2acesstat = new Serves(player2.getNickname(), player2serves);
            Serves Player3acesstat = new Serves(player3.getNickname(), player3serves);
            Serves Player4acesstat = new Serves(player4.getNickname(), player4serves);

            if (decide.equals("E")) {

                totalpointsqueue.add(Player1totalpointstat);
                totalpointsqueue.add(Player2totalpointstat);
                totalpointsqueue.add(Player3totalpointstat);
                totalpointsqueue.add(Player4totalpointstat);

                spikesqueue.add(Player1spikesstat);
                spikesqueue.add(Player2spikesstat);
                spikesqueue.add(Player3spikesstat);
                spikesqueue.add(Player4spikesstat);

                blocksqueue.add(Player1blocksstat);
                blocksqueue.add(Player2blocksstat);
                blocksqueue.add(Player3blocksstat);
                blocksqueue.add(Player4blocksstat);

                servesqueue.add(Player1acesstat);
                servesqueue.add(Player2acesstat);
                servesqueue.add(Player3acesstat);
                servesqueue.add(Player4acesstat);

                System.out.println("");

                int totalpointsqueuesize = totalpointsqueue.size();
                int spikesqueuesize = spikesqueue.size();
                int blocksqueuesize = blocksqueue.size();
                int servesqueuesize = servesqueue.size();

                System.out.println("SIMPLE AWARDING");
                System.out.println("");
                System.out.println("Spikes");
                int ranking = 1;
                for (int i = 1; i <= (spikesqueuesize); i++) {
                    
                    System.out.println("Top " + ranking + ": " + spikesqueue.remove().toString());

                    if (ranking == 4) {
                        System.out.println("Congratiolations guys");
                        ranking = 1;
                    } else {
                        System.out.println("next");
                        ranking++;
                    }
                    
                }

                System.out.println("");
                System.out.println("Blocks");

                for (int i = 1; i <= (blocksqueuesize); i++) {
                    
                    System.out.println("Top " + ranking + ": " + blocksqueue.remove().toString());

                    if (ranking == 4) {
                        System.out.println("Congratiolations guys");
                        ranking = 1;
                    } else {
                        System.out.println("next");
                        ranking++;
                    }
                }

                System.out.println("");
                System.out.println("Service Aces");

                for (int i = 1; i <= (servesqueuesize); i++) {
                    
                    System.out.println("Top " + ranking + ": " + servesqueue.remove().toString());

                    if (ranking == 4) {
                        System.out.println("Congratiolations guys");
                        ranking = 1;
                    } else {
                        System.out.println("next");
                        ranking++;
                    }
                }

                System.out.println("");
                System.out.println("Total Points");
                for (int i = 1; i <= (totalpointsqueuesize); i++) {
                    
                    System.out.println("Top " + ranking + ": " + totalpointsqueue.remove().toString());

                    if (ranking == 4) {
                        System.out.println("Congratiolations guys");
                        ranking = 1;
                    } else {
                        System.out.println("next");
                        ranking++;
                    }
                }

                System.out.println("");

                break;

            } else if (decide.equals("S")) {
                while (true) {

                    int choice = random.nextInt(2);
                    System.out.println("Game " + game);

                    if (choice == 0) {
                        //SE wins//
                        System.out.println(team1.getTeamname() + " wins");

                    } else {
                        //Karasuno wins//
                        System.out.println(team2.getTeamname() + " wins");

                    }
                    System.out.println();
                    System.out.println("Player's Contributions on their particular team");

                    for (Profile profile : prof.allPlayers) {

                        int spikepoints = random.nextInt(22);
                        int blockpoints = random.nextInt(22);
                        int servepoints = random.nextInt(10);

                        int total = spikepoints + blockpoints + servepoints;
                        String name = profile.getNickname();
                        System.out.println("Player: " + profile.getNickname());
                        System.out.println("Spike points: " + spikepoints);
                        System.out.println("Block points: " + blockpoints);
                        System.out.println("Serve points: " + servepoints);

                        if (name.equals(player1.getNickname())) {

                            player1totalpoints += total;
                            player1spikes += spikepoints;
                            player1blocks += blockpoints;
                            player1serves += servepoints;

                        } else if (name.equals(player2.getNickname())) {

                            player2totalpoints += total;
                            player2spikes += spikepoints;
                            player2blocks += blockpoints;
                            player2serves += servepoints;

                        } else if (name.equals(player3.getNickname())) {
                            player3totalpoints += total;
                            player3spikes += spikepoints;
                            player3blocks += blockpoints;
                            player3serves += servepoints;
                        } else {
                            player4totalpoints += total;
                            player4spikes += spikepoints;
                            player4blocks += blockpoints;
                            player4serves += servepoints;
                        }
                        System.out.println("");

                    }
                    game++;
                    System.out.println("Continue to Game " + game + "?");
                    break;

                }

            } else {
                System.out.println("Wrong Key");

            }
        }

    }
}
